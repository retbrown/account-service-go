module bitbucket.org/retbrown/account-service-go

require (
	github.com/go-chi/chi v4.0.0+incompatible
	github.com/go-chi/render v1.0.1
	github.com/google/uuid v1.1.0
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.0.0
	golang.org/x/net v0.0.0-20190110200230-915654e7eabc // indirect
	golang.org/x/text v0.3.0 // indirect
)
