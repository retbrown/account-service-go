package transfer

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"os"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/google/uuid"
	_ "github.com/lib/pq"
)

var db *sql.DB

type account struct {
	ID      uuid.UUID
	Balance int64
}

type transferType struct {
	ID          uuid.UUID `json:"id"`
	Source      uuid.UUID `json:"source"`
	Destination uuid.UUID `json:"destination"`
	Value       int64     `json:"value"`
}

// Routes function provides routes for this package
func Routes() *chi.Mux {
	var err error
	connStr := os.Getenv("CONN_STRING")
	db, err = sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}

	router := chi.NewRouter()
	router.Post("/", transfer)
	return router
}

func transfer(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var t transferType
	err := decoder.Decode(&t)
	if err != nil {
		panic(err)
	}

	if t.Value < 1 {
		render.JSON(w, r, "{\"message\": \"Value cannot be 0 or negative\"}")
		return
	}

	var present int
	err = db.QueryRow(`SELECT count(*) from accounts where id in ($1, $2)`, t.Source, t.Destination).Scan(&present)
	if err != nil {
		log.Fatal(err)
	}

	if present != 2 {
		render.JSON(w, r, "{\"message\": \"Account not found\"}")
		return
	}

	sourceAccount, err := findAccount(t.Source)
	if err != nil {
		log.Fatal(err)
	}

	destinationAccount, err := findAccount(t.Destination)
	if err != nil {
		log.Fatal(err)
	}

	if sourceAccount.Balance < t.Value {
		render.JSON(w, r, `{"message": "Insufficient funds"}`)
		return
	}

	sourceAccount.Balance = sourceAccount.Balance - t.Value
	destinationAccount.Balance = destinationAccount.Balance + t.Value

	t.ID, err = uuid.NewRandom()

	sqlStatement := "INSERT into transfers (id, source, destination, value) values ($1, $2, $3, $4)"
	_, err = db.Exec(sqlStatement, t.ID, t.Source, t.Destination, t.Value)
	if err != nil {
		log.Fatal(err)
	}

	err = updateAccount(sourceAccount.ID, sourceAccount.Balance)
	if err != nil {
		log.Fatal(err)
	}

	err = updateAccount(destinationAccount.ID, destinationAccount.Balance)
	if err != nil {
		log.Fatal(err)
	}

	render.JSON(w, r, t)
}

func updateAccount(id uuid.UUID, newBalance int64) error {
	sqlStatement := "Update accounts set balance = $1 where id = $2"
	_, err := db.Exec(sqlStatement, newBalance, id)
	if err != nil {
		return err
	}

	return nil
}

func findAccount(id uuid.UUID) (account, error) {
	var Account account
	rows, err := db.Query(`SELECT id, balance FROM accounts WHERE id = $1`, id)
	if err != nil {
		return Account, err
	}

	defer rows.Close()
	for rows.Next() {
		var id string
		var balance int64
		err = rows.Scan(&id, &balance)

		Account.ID, err = uuid.Parse(id)
		if err != nil {
			return Account, err
		}
		Account.Balance = balance
	}

	return Account, nil
}
